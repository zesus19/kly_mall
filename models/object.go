package models

const (
	CATEGORY_CACHE_KEY           = "CATEGORY_CACHE_KEY"
	CATEGORY_ATTRIBUTE_CACHE_KEY = "CATEGORY_ATTRIBUTE_CACHE_KEY"
)

type Category struct {
	ID          int64  `json:"id"`
	ParentID    int64  `json:"parent_id"`
	Name        string `json:"name"`
	Desc        string `json:"desc"`
	MeasureUnit string `json:"measure_unit"`
}

type Attribute struct {
	ID        int64  `json:"id"`
	Name      string `json:"name"`
	InputType int    `json:"input_type"`
	Type      int    `json:"type"`
	Values    string `json:"values"`
}

type GoodsList struct {
	TotalCount int64         `json:"total_count"`
	List       []interface{} `json:"goods_list"`
}

type Goods struct {
	ID            int64   `json:"id"`
	CategoryID    int64   `json:"cat_id"`
	SN            string  `json:"sn"`
	Name          string  `json:"name"`
	NameStyle     string  `json:"name_style"`
	ClickCount    int     `json:"click_count"`
	Number        int     `json:"number"`
	Weight        float32 `json:"weight"`
	MarketPrice   int     `json:"market_price"`
	ShopPrice     int     `json:"shop_price"`
	Brief         string  `json:"brief"`
	Desc          string  `json:"desc"`
	Thumb         string  `json:"thumb"`
	Img           string  `json:"img"`
	OriginalImg   string  `json:"original_img"`
	IsReal        int     `json:"is_real"`
	ExtensionCode string  `json:"extension_code"`
	IsShipping    int     `json:"is_shipping"`
	IsBest        int     `json:"is_best"`
	IsNew         int     `json:"is_new"`
	IsHot         int     `json:"is_hot"`
}

type GoodsAttribute struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

type MakeOrderRQ struct {
	GoodsID      int64  `json:"goods_id"`
	Consignee    string `json:"consignee"`
	Country      string `json:"country"`
	Province     string `json:"province"`
	City         string `json:"city"`
	District     string `json:"district"`
	Address      string `json:"address"`
	ZipCode      string `json:"zipcode"`
	Tel          string `json:"tel"`
	Mobile       string `json:"mobile"`
	Email        string `json:"email"`
	Postscript   string `json:"postscript"`
	ShippingID   int    `json:"shipping_id"`
	ShippingName string `json:"shipping_name"`
	ShippingCode string `json:"shipping_code"`
	GoodsNumber  int    `json:"goods_number"`
}

type Order struct {
	ID             int64  `json:"id"`
	NO             string `json:"no"`
	Status         int    `json:"order_status"`
	ShippingStatus int    `json:"shipping_status"`
	PayStatus      int    `json:"pay_status"`
	Consignee      string `json:"consignee"`
	Country        string `json:"country"`
	Province       string `json:"province"`
	City           string `json:"city"`
	District       string `json:"district"`
	Address        string `json:"address"`
	ZipCode        string `json:"zipcode"`
	Tel            string `json:"tel"`
	Mobile         string `json:"mobile"`
	Email          string `json:"email"`
	Postscript     string `json:"postscript"`
	ShippingName   string `json:"shipping_name"`
	ShippingCode   string `json:"shipping_code"`
	Amount         int    `json:"goods_amount"`
	AddTime        string `json:"add_time"`
}

type OrderInfo struct {
	NO             string `json:"no"`
	Status         int    `json:"order_status"`
	ShippingStatus int    `json:"shipping_status"`
	PayStatus      int    `json:"pay_status"`
	Amount         int    `json:"goods_amount"`
	PayFee         int    `json:"pay_fee"`
	AddTime        string `json:"add_time"`
	GoodsName      string `json:"goods_name"`
	GoodsThumb     string `json:"goods_thumb"`
}

type OrderList struct {
	TotalCount int64       `json:"total_count"`
	List       interface{} `json:"order_list"`
}

type ShippingInfo struct {
	Consignee    string `json:"consignee"`
	Country      string `json:"country"`
	Province     string `json:"province"`
	City         string `json:"city"`
	District     string `json:"district"`
	Address      string `json:"address"`
	ZipCode      string `json:"zipcode"`
	Tel          string `json:"tel"`
	Mobile       string `json:"mobile"`
	Email        string `json:"email"`
	ShippingID   int    `json:"shipping_id"`
	ShippingName string `json:"shipping_name"`
	ShippingCode string `json:"shipping_code"`
}

type GoodsImage struct {
	Thumb    string `json:"goods_thumb"`
	Large    string `json:"goods_img"`
	Original string `json:"original_img"`
}
