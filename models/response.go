package models

type Response struct {
	OK      bool        `json:"ok"`
	Message string      `json:"message,omitempty"`
	Result  interface{} `json:"result,omitempty"`
}

func NewOkResponse(result interface{}) Response {
	return Response{OK: true, Result: result}
}

func NewFailResponse(err error) Response {
	return Response{OK: false, Message: err.Error()}
}
