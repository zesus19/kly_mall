package models

import (
	"github.com/astaxie/beego"
)

func RedisAddr() string {
	return beego.AppConfig.DefaultString("redisServer", "120.26.223.135:22359")
}
