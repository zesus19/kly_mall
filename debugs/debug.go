package debugs

import "bytes"
import "fmt"

type Debug interface {
	Name() string
	Append(key string, value interface{}) Debug
	Level() int
	Format() string
}

type debugImpl struct {
	name string
	data map[string]interface{}
}

func (debug *debugImpl) Name() string {
	return debug.name
}

func (debug *debugImpl) Append(key string, value interface{}) Debug {
	debug.data[key] = value
	return debug
}

func (debug *debugImpl) Level() int {
	return 7
}

func (debug *debugImpl) Format() string {
	var buffer bytes.Buffer
	for k, v := range debug.data {
		buffer.WriteString(fmt.Sprintf("<%s", k))
		buffer.WriteString(":")
		buffer.WriteString(fmt.Sprintf("%v", v))
		buffer.WriteString(">")
	}
	return fmt.Sprintf("%s::%s", debug.Name(), buffer.String())
}

func New(name string) Debug {
	return &debugImpl{name: name, data: make(map[string]interface{})}
}
