package filters

import (
	"gitchina/kly_mall/debugs"
	"gitchina/kly_mall/util"
	"strings"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context"
)

const (
	FUNC_FILTER_DEBUG = "filter.debug"
)

var DebugFilterFunc = func(ctx *context.Context) {
	if beego.BConfig.RunMode != "prod" {
		contentType := ctx.Request.Header.Get("Content-Type")
		if !strings.Contains(contentType, "json") {
			util.WriteLog(debugs.New(FUNC_FILTER_DEBUG).Append("url", ctx.Input.URL()))
		} else {
			util.WriteLog(debugs.
				New(FUNC_FILTER_DEBUG).
				Append("url", ctx.Input.URL()).
				Append("body", string(ctx.Input.RequestBody)))
		}
		util.WriteLog(debugs.New(FUNC_FILTER_DEBUG).Append("header", ctx.Request.Header))
	}
}
