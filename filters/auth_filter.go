package filters

import (
	"gitchina/kly_mall/models"
	"net/http"

	_ "github.com/astaxie/beego"
	"github.com/astaxie/beego/context"
)

var auth_map = map[string]byte{
	"101005": 1,
	"101006": 1,
	"101007": 1,
	"101008": 1,
	"101009": 1,
}

var AuthFilterFunc = func(ctx *context.Context) {
	id, ok := ctx.Input.Params()[":id"]
	if ok {
		if _, ok := auth_map[id]; ok {
			token := ctx.Request.Header.Get("Auth-Token")
			if pass, mbrId, phone, nickName := models.Authenticate(token); !pass {
				http.Error(ctx.ResponseWriter, "", http.StatusUnauthorized)
			} else {
				ctx.Input.SetData("mbrId", mbrId)
				ctx.Input.SetData("phone", phone)
				ctx.Input.SetData("nickName", nickName)
			}
		}
	}
}
