package data

import "github.com/astaxie/beego/orm"

type Category struct {
	ID          int64  `orm:"pk;auto;column(cat_id)"`
	Name        string `orm:"column(cat_name)"`
	Desc        string `orm:"column(cat_desc)"`
	ParentID    int64  `orm:"column(parent_id)"`
	SortOrder   int    `orm:"column(sort_order)"`
	MeasureUnit string `orm:"column(measure_unit)"`
}

type Attribute struct {
	ID         int64  `orm:"pk;auto;column(attr_id)"`
	CategoryID int64  `orm:"column(cat_id)"`
	Name       string `orm:"column(attr_name)"`
	InputType  int    `orm:"column(attr_input_type)"`
	Type       int    `orm:"column(attr_type)"`
	Values     string `orm:"column(attr_values)"`
	SortOrder  int    `orm:"column(sort_order)"`
}

type Goods struct {
	ID            int64   `orm:"pk;auto;column(goods_id)"`
	CategoryID    int64   `orm:"column(cat_id)"`
	SN            string  `orm:"column(goods_sn)"`
	Name          string  `orm:"column(goods_name)"`
	NameStyle     string  `orm:"column(goods_name_style)"`
	ClickCount    int     `orm:"column(click_count)"`
	BrandID       int     `orm:"column(brand_id)"`
	Number        int     `orm:"column(goods_number)"`
	Weight        float32 `orm:"column(goods_weight)"`
	MarketPrice   int     `orm:"column(market_price)"`
	ShopPrice     int     `orm:"column(shop_price)"`
	WarnNumber    int     `orm:"column(warn_number)"`
	Keywords      string  `orm:"column(keywords)"`
	Brief         string  `orm:"column(goods_brief)"`
	Desc          string  `orm:"column(goods_desc)"`
	Thumb         string  `orm:"column(goods_thumb)"`
	Img           string  `orm:"column(goods_img)"`
	OriginalImg   string  `orm:"column(original_img)"`
	IsReal        int     `orm:"column(is_real)"`
	ExtensionCode string  `orm:"column(extension_code)"`
	IsOnSale      int     `orm:"column(is_on_sale)"`
	IsShipping    int     `orm:"column(is_shipping)"`
	AddTime       string  `orm:"column(add_time)"`
	SortOrder     int     `orm:"column(sort_order)"`
	IsBest        int     `orm:"column(is_best)"`
	IsNew         int     `orm:"column(is_new)"`
	IsHot         int     `orm:"column(is_hot)"`
	IsDelete      int     `orm:"column(is_delete)"`
	IsCheck       int     `orm:"column(is_check)"`
	LastUpdate    string  `orm:"column(last_update)"`
}

type GoodsAttribute struct {
	Name  string `orm:"column(attr_name)"`
	Value string `orm:"column(attr_value)"`
}

type Order struct {
	ID             int64  `orm:"pk;auto;column(order_id)"`
	NO             string `orm:"column(order_sn)"`
	UserID         string `orm:"column(user_id)"`
	Status         int    `orm:"column(order_status)"`
	ShippingStatus int    `orm:"column(shipping_status)"`
	PayStatus      int    `orm:"column(pay_status)"`
	Consignee      string `orm:"column(consignee)"`
	Country        string `orm:"column(country)"`
	Province       string `orm:"column(province)"`
	City           string `orm:"column(city)"`
	District       string `orm:"column(district)"`
	Address        string `orm:"column(address)"`
	ZipCode        string `orm:"column(zipcode)"`
	Tel            string `orm:"column(tel)"`
	Mobile         string `orm:"column(mobile)"`
	Email          string `orm:"column(email)"`
	Postscript     string `orm:"column(postscript)"`
	ShippingID     int    `orm:"column(shipping_id)"`
	ShippingName   string `orm:"column(shipping_name)"`
	ShippingCode   string `orm:"column(shipping_code)"`
	Amount         int    `orm:"column(goods_amount)"`
	PayFee         int    `orm:"column(pay_fee)"`
	AddTime        string `orm:"column(add_time)"`
}

type OrderGoods struct {
	ID            int64  `orm:"pk;auto;column(rec_id)"`
	OrderID       int64  `orm:"column(order_id)"`
	GoodsID       int64  `orm:"column(goods_id)"`
	Name          string `orm:"column(goods_name)"`
	SN            string `orm:"column(goods_sn)"`
	Number        int    `orm:"column(goods_number)"`
	MarketPrice   int    `orm:"column(market_price)"`
	GoodsPrice    int    `orm:"column(goods_price)"`
	GoodsAttr     string `orm:"column(goods_attr)"`
	SendNumber    int    `orm:"column(send_number)"`
	IsReal        int    `orm:"column(is_real)"`
	ExtensionCode string `orm:"column(extension_code)"`
}

type OrderInfo struct {
	ID             int64  `orm:"pk;auto;column(order_id)"`
	NO             string `orm:"column(order_sn)"`
	Status         int    `orm:"column(order_status)"`
	ShippingStatus int    `orm:"column(shipping_status)"`
	PayStatus      int    `orm:"column(pay_status)"`
	Amount         int    `orm:"column(goods_amount)"`
	PayFee         int    `orm:"column(pay_fee)"`
	AddTime        string `orm:"column(add_time)"`
	GoodsName      string `orm:"column(goods_name)"`
	GoodsThumb     string `orm:"column(goods_thumb)"`
}

type PayLog struct {
	ID      int64 `orm:"pk;auto;column(log_id)"`
	OrderID int64 `orm:"column(order_id)"`
	Amount  int   `orm:"column(order_amount)"`
	Type    int   `orm:"column(order_type)"`
	IsPaid  int   `orm:"column(is_paid)"`
}

func init() {
	orm.RegisterModel(
		new(Category),
		new(Attribute),
		new(Goods),
		new(Order),
		new(OrderGoods),
		new(PayLog))
}

func (c *Category) TableName() string {
	return "e_category"
}

func (attr *Attribute) TableName() string {
	return "e_attribute"
}

func (goods *Goods) TableName() string {
	return "e_goods"
}

func (order *Order) TableName() string {
	return "e_order_info"
}

func (goods *OrderGoods) TableName() string {
	return "e_order_goods"
}

func (log *PayLog) TableName() string {
	return "e_pay_log"
}
