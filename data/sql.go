package data

import (
	"gitchina/kly_mall/util"

	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)

const (
	SQL_REDUCE_GOODS_AMOUNT = "UPDATE e_goods SET goods_number = goods_number - ? WHERE goods_number >= ? AND goods_id = ?"
	SQL_FINISH_ORDER        = "UPDATE e_order_info SET order_status = ?, pay_status = ?, confirm_time = ?, pay_time = ? WHERE order_id = ?"
	SQL_REDUCE_USER_POINT   = "UPDATE m_member SET point = point - ? WHERE point >= ? AND id = ?"
	SQL_UPDATE_GOODS_IMG    = "UPDATE e_goods SET goods_thumb = ?, goods_img = ?, original_img = ? WHERE goods_id = ?"
	SQL_GET_GOODS_ATTR      = "SELECT attr_name, attr_value FROM e_attribute e INNER JOIN e_goods_attr g ON e.attr_id = g.attr_id WHERE g.goods_id = ?"
	SQL_GET_ORDER_LIST      = "SELECT o.order_id, order_sn, order_status, shipping_status, pay_status, goods_amount, pay_fee, o.add_time, og.goods_name, goods_thumb FROM e_order_info o INNER JOIN e_order_goods og ON o.order_id = og.order_id INNER JOIN e_goods g ON og.goods_id = g.goods_id WHERE user_id = ? LIMIT ?, ?"
)

var shopmallConnStr string
var klyConnStr string

func Init(runmode string) {
	if runmode != "prod" {
		orm.Debug = true
		if runmode == "uat" {
			klyConnStr = "prod_klydb:@WW%_)2a@tcp(rm-bp10j3s1681i41x20.mysql.rds.aliyuncs.com:3306)/KlyDB?charset=utf8"
			shopmallConnStr = "prod_klydb:@WW%_)2a@tcp(rm-bp10j3s1681i41x20.mysql.rds.aliyuncs.com:3306)/kly_shopdb?charset=utf8"
		} else if runmode == "test" {
			klyConnStr = "test:123456@tcp(120.26.223.135:22479)/KlyDB?charset=utf8"
			shopmallConnStr = "test:123456@tcp(120.26.223.135:22479)/KlyShopDB?charset=utf8"
		} else {
			klyConnStr = "root:123456@tcp(127.0.0.1:3306)/KlyDB?charset=utf8"
			shopmallConnStr = "root:123456@tcp(127.0.0.1:3306)/KlyShopDB?charset=utf8"
		}
	}
	orm.DebugLog = orm.NewLog(util.UseBeeLogger())
	orm.RegisterDriver("mysql", orm.DRMySQL)
	orm.RegisterDataBase("default", "mysql", shopmallConnStr)
	orm.RegisterDataBase("kly", "mysql", klyConnStr)
}

func GetShopmallConnStr() string {
	return shopmallConnStr
}

func GetKlyConnStr() string {
	return klyConnStr
}
