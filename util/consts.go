package util

const (
	Ios_H5                  = "001"
	Android_H5              = "002"
	Recommend_Line_Cache    = "RLC"
	General_Cache           = "GC"
	Ad_Cache_Key            = "ACK"
	Param_Cache_Key         = "PCK"
	Dict_Cache_Key          = "DCK"
	Dict_Cache_Key2         = "DCK2"
	IP_Block_List_Cache_Key = "IBLCK"
	Bank_List_Cache_Key     = "BLCK"
)


