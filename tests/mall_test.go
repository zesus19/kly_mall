package tests

import (
	"encoding/json"
	"fmt"
	"gitchina/kly_mall/models"
	"gitchina/kly_mall/tests/common"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

var orderNO string

func init() {
	common.InitCommon(1)
	common.TruncateTables("e_order_info", "e_order_goods", "e_pay_log")
	common.SqlExec("update e_goods set goods_number = 100")
	common.InitCommon(2)
	common.SqlExec("update m_member set point = 301")
}

func TestAll(t *testing.T) {
	fmt.Println("====================================================================================================================================================================================")
	testGetGoods(t)
	testGetGoodsAttrs(t)
	testMakeOrder(t)
	testGetOrder(t)
	testGetOrderList(t)
	testCharge(t)
	testGetShippingInfo(t)
}

func testGetGoods(t *testing.T) {
	Convey("test get goods", t, func() {
		var mall models.Mall
		list := mall.GetGoodsList(1, 1, 20)
		So(list.TotalCount, ShouldEqual, 6)
		list = mall.GetGoodsList(2, 1, 20)
		So(list.TotalCount, ShouldEqual, 1)
		list = mall.GetGoodsList(-1, 1, 20)
		So(list.TotalCount, ShouldEqual, 7)
	})
}

func testGetGoodsAttrs(t *testing.T) {
	Convey("test get goods attrs", t, func() {
		var mall models.Mall
		attrs := mall.GetGoodsAttrs(1).([]interface{})
		So(len(attrs), ShouldEqual, 3)
		p, err := json.Marshal(&attrs)
		So(err, ShouldEqual, nil)
		fmt.Println(string(p))
	})
}

func testMakeOrder(t *testing.T) {
	Convey("Test make order", t, func() {
		var memberID models.ID
		memberID.Encode(1, 1, 1, 1)
		rq := models.MakeOrderRQ{
			GoodsID:      1,
			Consignee:    "a",
			Country:      "111",
			Province:     "111",
			City:         "111",
			District:     "111",
			Address:      "111",
			ZipCode:      "111",
			Tel:          "111",
			Mobile:       "111",
			Email:        "111",
			Postscript:   "111",
			ShippingID:   1,
			ShippingName: "111",
			ShippingCode: "111",
			GoodsNumber:  2,
		}
		var err error
		var mall models.Mall
		orderNO, err = mall.MakeOrder(memberID, rq)
		So(err, ShouldEqual, nil)
		So(orderNO, ShouldNotEqual, "")
	})
}

func testGetOrder(t *testing.T) {
	Convey("Test get order", t, func() {
		var memberID models.ID
		memberID.Encode(1, 1, 1, 1)
		var mall models.Mall
		order, err := mall.GetOrder(orderNO, string(memberID))
		So(err, ShouldEqual, nil)
		fmt.Println(fmt.Sprintf("%v", order))
	})
}

func testGetOrderList(t *testing.T) {
	Convey("Test get order list", t, func() {
		var memberID models.ID
		memberID.Encode(1, 1, 1, 1)
		var mall models.Mall
		list := mall.GetOrderList(string(memberID), 1, 2)
		So(list.TotalCount, ShouldEqual, 1)
		orders := list.List.([]interface{})
		So(len(orders), ShouldEqual, 1)
		p, _ := json.Marshal(&orders)
		fmt.Println(string(p))
	})
}

func testCharge(t *testing.T) {
	Convey("Test charge", t, func() {
		var memberID models.ID
		memberID.Encode(1, 1, 1, 1)
		var mall models.Mall
		err := mall.Charge(orderNO, memberID)
		So(err, ShouldEqual, nil)
	})
}

func testGetShippingInfo(t *testing.T) {
	Convey("Test get shipping info", t, func() {
		var memberID models.ID
		memberID.Encode(1, 1, 1, 1)
		var mall models.Mall
		info := mall.GetShippingInfo(string(memberID))
		fmt.Println(fmt.Sprintf("%v", info))
	})
}
