package main

import (
	"gitchina/kly_mall/controllers"
	"gitchina/kly_mall/data"
	"gitchina/kly_mall/filters"
	"gitchina/kly_mall/util"
	"os"

	_ "gitchina/kly_mall/routers"

	"github.com/astaxie/beego"
)

func main() {
	runmode := os.Getenv("KLYMALLRUNMODE")
	beego.BConfig.RunMode = runmode
	util.InitLogger(beego.BConfig.RunMode)
	data.Init(beego.BConfig.RunMode)

	if beego.BConfig.RunMode != "prod" {
		beego.BConfig.Log.AccessLogs = true
		beego.BConfig.WebConfig.DirectoryIndex = true
		beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"

	}

	if beego.BConfig.RunMode != "dev" {
		adapter, config := util.GetLoggerProfile()
		beego.BConfig.Log.Outputs = map[string]string{adapter: config}
		beego.LoadAppConfig("ini", "conf/app.conf")
	}

	controllers.Init()

	beego.InsertFilter("/*", beego.BeforeRouter, filters.DebugFilterFunc)
	beego.InsertFilter("/*/api/:id*", beego.BeforeRouter, filters.AuthFilterFunc)
	beego.Run()
}
