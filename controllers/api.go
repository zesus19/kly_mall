package controllers

import (
	"encoding/json"
	"fmt"
	"gitchina/kly_mall/models"
	"gitchina/kly_mall/util"

	"github.com/astaxie/beego"
)

type ApiController struct {
	beego.Controller
}

// @router /101001 [get]
func (api *ApiController) GetCategories() {
	var mall models.Mall
	api.Data["json"] = models.NewOkResponse(mall.GetCategoryList())
	api.ServeJSON()
}

// @router /101002 [get]
func (api *ApiController) GetAttributes() {
	var mall models.Mall
	if categoryID, err := api.GetInt64("category_id", 0); err == nil {
		api.Data["json"] = models.NewOkResponse(mall.GetAttributeList(categoryID))
		api.ServeJSON()
	} else {
		api.Ctx.ResponseWriter.WriteHeader(400)
	}
}

// @router /101003 [get]
func (api *ApiController) GetGoodsList() {
	var mall models.Mall
	categoryID, _ := api.GetInt64("category_id", -1)
	pageNo, _ := api.GetInt("page_no", 1)
	pageSize, _ := api.GetInt("page_size", 10)
	api.Data["json"] = models.NewOkResponse(
		mall.GetGoodsList(categoryID, pageNo, pageSize))
	api.ServeJSON()
}

// @router /101004 [get]
func (api *ApiController) GetGoods() {
	var mall models.Mall
	goodsID, _ := api.GetInt64("goods_id", -1)
	api.Data["json"] = models.NewOkResponse(mall.GetGoods(goodsID))
	api.ServeJSON()
}

// @router /101005 [post]
func (api *ApiController) MakeOrder() {
	var mall models.Mall
	var rq models.MakeOrderRQ
	memberID := models.ID(api.Ctx.Input.GetData("mbrId").(string))
	if err := json.Unmarshal(api.Ctx.Input.RequestBody, &rq); err == nil {
		if no, err := mall.MakeOrder(memberID, rq); err == nil {
			api.Data["json"] = models.NewOkResponse(no)
		} else {
			api.Data["json"] = models.NewOkResponse(err)
		}
		api.ServeJSON()
	} else {
		api.Ctx.ResponseWriter.WriteHeader(400)
	}
}

// @router /101006 [post]
func (api *ApiController) GetShippingInfo() {
	var mall models.Mall
	api.Data["json"] = models.NewOkResponse(mall.GetShippingInfo(api.Ctx.Input.GetData("mbrId").(string)))
	api.ServeJSON()
}

// @router /101007 [post]
func (api *ApiController) GetOrderInfo() {
	var mall models.Mall
	orderNO := api.GetString("order_id", "")
	memberID := api.Ctx.Input.GetData("mbrId").(string)
	if order, err := mall.GetOrder(orderNO, memberID); err == nil {
		api.Data["json"] = models.NewOkResponse(order)
	} else {
		api.Data["json"] = models.NewFailResponse(err)
	}
	api.ServeJSON()
}

// @router /101008 [post]
func (api *ApiController) Charge() {
	var mall models.Mall
	orderNO := api.GetString("order_id", "")
	memberID := models.ID(api.Ctx.Input.GetData("mbrId").(string))
	if err := mall.Charge(orderNO, memberID); err == nil {
		api.Data["json"] = models.NewOkResponse(nil)
	} else {
		api.Data["json"] = models.NewFailResponse(err)
	}
	api.ServeJSON()
}

// @router /101009 [post]
func (api *ApiController) GetOrderList() {
	var mall models.Mall
	pageNo, _ := api.GetInt("page_no", 1)
	pageSize, _ := api.GetInt("page_size", 10)
	memberID := api.Ctx.Input.GetData("mbrId").(string)
	list := mall.GetOrderList(memberID, pageNo, pageSize)
	api.Data["json"] = models.NewOkResponse(list)
	api.ServeJSON()
}

// @router /101010 [get]
func (api *ApiController) GetGoodsAttrs() {
	var mall models.Mall
	goodsID, _ := api.GetInt64("goods_id", -1)
	attrs := mall.GetGoodsAttrs(goodsID)
	api.Data["json"] = models.NewOkResponse(attrs)
	api.ServeJSON()
}

// @router /201001 [get]
func (api *ApiController) RefreshCategories() {
	util.Event{Name: util.NOTIFY_CATEGORY_LOAD}.Pub()
}

// @router /201002 [get]
func (api *ApiController) RefreshAttributes() {
	categoryID, _ := api.GetInt64("category_id", 0)
	util.Event{Name: util.NOTIFY_ATTRIBUTE_LOAD, Data: categoryID}.Pub()
}

// @router /301001 [post]
func (api *ApiController) UploadImage() {
	var mall models.Mall
	if file, header, err := api.GetFile("uploadimg"); err == nil {
		if img, code := mall.UploadImg(
			beego.AppConfig.String("imageCdn"),
			beego.AppConfig.String("ossBucketName"),
			beego.AppConfig.String("storeLocation"),
			beego.AppConfig.DefaultInt("maxImageSize", 10485760),
			file, header); code == 200 {
			api.Data["json"] = models.NewOkResponse(img)
			api.ServeJSON()
		} else {
			api.Ctx.ResponseWriter.WriteHeader(code)
		}
	} else {
		fmt.Println(err.Error())
	}
}
