package controllers

import (
	"gitchina/kly_mall/models"
)

func Init() {
	models.PreloadInit()
}
